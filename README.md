# Car Rental App


# Notes

1. Mapkit started sending logs just before submitting my app. Could not sit over it

2.  Alamofire response for the .POST request always threw some "Internal Server Error"
Changed the request to NSURLRequest with .post method


# Could have done better:


1. Could have used or designed  Generic data source DataSource<T> in a better way to be used by TableView data source.

```
class DataSource<T> : NSObject {
var data : [T] = []
}
```

2. Due to the unexpected response from the Alamofire post request could not take full advantage of Service protocol class
```protocol DataServiceProtocol {...}```

3. Could have used better image for the Car pins and could display in a rotated angle w.r.t to the direction from the coordinates

4.  Could have displayed items in the liste divided into sections with relatable fields

5. Could have looked for some mapping mechanism to Map the received Selectedcar object to an Array.
```
class SelectedCarViewModel {
func fetchSelectedCarDetails(...){

let item = data as! SelectedCar // from response and map to an array
}
}
```
